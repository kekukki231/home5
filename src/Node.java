import java.util.StringTokenizer;

public class Node {

    private String name;
    private Node firstChild;
    private Node nextSibling;

    Node(String n, Node d, Node r) {
        setName(n);
        setFirstChild(d);
        setNextSibling(r);
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setFirstChild(Node firstChild) {
        this.firstChild = firstChild;
    }

    public void setNextSibling(Node nextSibling) {
        this.nextSibling = nextSibling;
    }

    public static boolean isParsable(String s) {
        return (s != null && !s.isEmpty());
    }

    private static void verifyString(String s) {
        if (s.equals("\t"))
            throw new RuntimeException("Nodes name can't contain tabs!");

        if (s.contains(" ")) {
            throw new RuntimeException("Nodes name can't contain spaces!");
        }
        if (s.contains(",,")) {
            throw new RuntimeException("Side by side commas aren't allowed!");
        }
        if (s.charAt(0) == ')' || s.charAt(s.length() - 1) == '(') {
            throw new RuntimeException("Incorrect parenthesis placement!");
        }
    }

    private static boolean isSuccessor(String s, boolean isSucc) {
        String lastElem = new String(s).substring(s.length() - 1);
        if (lastElem.matches("[)]")) {
            isSucc = true;
        }
        return isSucc;
    }

    private static void replaceWith(StringBuffer oldSb, StringBuffer newSb) {
        oldSb.setLength(0);
        oldSb.append(newSb);
    }

    private static void replaceWithString(StringBuffer oldSb, String newString) {
        oldSb.replace(0, oldSb.length(), newString);
    }

    @Override
    public String toString() {
        return "Node{" +
                "name='" + name + '\'' +
                ", firstChild=" + firstChild +
                ", nextSibling=" + nextSibling +
                '}';
    }

    public static Node parsePostfix(String s) {

        if (!Node.isParsable(s)) {
            return null;
        }

        verifyString(s);

        boolean isSucc = false;
        isSucc = isSuccessor(s, isSucc);

        StringTokenizer st = new StringTokenizer(s, ",()", true);
        StringBuffer temp = new StringBuffer();
        StringBuffer node = new StringBuffer();
        StringBuffer d = new StringBuffer();
        StringBuffer r = new StringBuffer();

        while (st.hasMoreTokens()) {
            replaceWithString(temp, st.nextToken());

            if (temp.toString().equals("(")) {

                if (isSucc && r.length() == 0) {
                    replaceWithString(temp, st.nextToken());
                    int closedD = 0;
                    if (temp.toString().equals("(")) {
                        while (closedD < 1) {
                            d.append(temp);
                            replaceWithString(temp, st.nextToken());
                            StringTokenizer stTemp = new StringTokenizer(d.toString(), ",()", true);
                            if (temp.toString().equals("(")) {
                                closedD = closedD - 1;
                            } else if (temp.toString().equals(")")) {
                                closedD = closedD + 1;
                                d.append(temp);
                                replaceWithString(temp, st.nextToken());
                            } else if (closedD == 0 && temp.toString().equals(",") && stTemp.countTokens() < 1) {
                                closedD = closedD + 1;
                            }
                        }
                    }
                    if (node.length() == 0) {
                        replaceWith(node, temp);
                        temp.setLength(0);
                    }
                }

                int closed = 0;
                while (closed < 1) {
                    d.append(temp);
                    replaceWithString(temp, st.nextToken());
                    if (temp.toString().equals("(")) {
                        closed = closed - 1;
                    } else if (temp.toString().equals(")")) {
                        if (isSucc && r.length() != 0) {
                            isSucc = false;
                            temp.setLength(0);
                        }
                        if (closed == 0 && node.length() == 0) {
                            d.append(temp);
                        }
                        closed = closed + 1;
                    } else if (isSucc) {
                        if (temp.toString().equals(",")) {
                            replaceWithString(temp, st.nextToken());
                        }
                        replaceWith(r, temp);
                        int count = st.countTokens();
                        while (count > 1) {
                            r.append(st.nextToken());
                            count = st.countTokens();
                        }
                        if (r.charAt(r.length() - 1) != ')') {
                            replaceWith(temp, r);
                            r.setLength(0);
                            r.append("(").append(temp).append(")");
                        }
                        temp.setLength(0);
                    }
                }
            }

            if (st.hasMoreElements()) {
                while (st.hasMoreTokens()) {
                    node.append(st.nextToken());
                }
            } else if (node.length() == 0) {
                node = temp;
            }
        }

        if (node.toString().contains(",")) {
            throw new RuntimeException("Misplaced comma in node's name");
        }

        return new Node(node.toString(), Node.parsePostfix(d.toString()), Node.parsePostfix(r.toString()));
    }

    public String leftParentheticRepresentation() {
        StringBuffer sb = new StringBuffer();
        sb.append(name);
        if (firstChild != null) {
            sb.append("(").append(firstChild.leftParentheticRepresentation()).append(")");
        }
        if (nextSibling != null) {
            sb.append(",").append(nextSibling.leftParentheticRepresentation());
        }
        return sb.toString();
    }

    public static void main(String[] param) {
        String s = "(B1,C)A";
        Node t = Node.parsePostfix(s);
        //System.out.println(t);
        String v = t.leftParentheticRepresentation();
        System.out.println(s + " ==> " + v); // (B1,C)A ==> A(B1,C)
    }
}

